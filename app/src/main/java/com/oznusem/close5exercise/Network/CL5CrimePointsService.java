package com.oznusem.close5exercise.Network;


import com.oznusem.close5exercise.Models.Crime;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by Oz Nusem on 1/9/16.
 */
public interface CL5CrimePointsService {

    @GET("/resource/cuks-n6tp.json")
    Observable<List<Crime>> listCL5CrimeLocation(@Query("$where") String between);

}
