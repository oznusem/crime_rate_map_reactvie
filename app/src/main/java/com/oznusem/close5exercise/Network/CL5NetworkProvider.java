package com.oznusem.close5exercise.Network;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by Oz Nusem on 1/9/16.
 */
public class CL5NetworkProvider {

    private static final String PRODUCTION_URL = "https://data.sfgov.org";

    private static CL5NetworkProvider instance = new CL5NetworkProvider();
    private static CL5CrimePointsService cl5CrimePointsService;

    public static CL5NetworkProvider getInstance() {
        return instance;
    }

    private CL5NetworkProvider() {
    }

    public CL5CrimePointsService getCL5CrimePointsService() {
        if (cl5CrimePointsService == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(PRODUCTION_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            cl5CrimePointsService = retrofit.create(CL5CrimePointsService.class);
        }
        return cl5CrimePointsService;
    }

}
