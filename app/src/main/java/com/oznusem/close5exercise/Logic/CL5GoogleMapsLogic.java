package com.oznusem.close5exercise.Logic;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.oznusem.close5exercise.Models.District;

import java.util.Collection;

/**
 * Created by Oz Nusem on 1/10/16.
 */
public class CL5GoogleMapsLogic {


    public static void markDistrictOnMap(Collection<District> districtList, GoogleMap map) {
        for (District district  : districtList) {
            CL5GoogleMapsLogic.markPoint(district,map);
        }
    }

    public static void markPoint(District district, GoogleMap map) {

        if(district.getLocation() == null) {
            return;
        }

        LatLng latLng = new LatLng(district.getLocation().getLatitude(), district.getLocation().getLongitude());
        map.addMarker(new MarkerOptions()
                .position(latLng)
                .title(district.getName())
                .icon(BitmapDescriptorFactory.defaultMarker(getHueByCrimeOrder(district.getOrder()))));
    }


    public static void animateCameraToPoint(double lat, double lon, int zoom, GoogleMap map) {

        CameraPosition camPos = new CameraPosition.Builder()
                .target(new LatLng(lat,lon))
                .zoom(zoom)
                .build();
        CameraUpdate camUpd3 = CameraUpdateFactory.newCameraPosition(camPos);
        map.animateCamera(camUpd3);
    }

    public static Float getHueByCrimeOrder(int crimeOrder) {

        switch (crimeOrder) {
            case 0:
                return BitmapDescriptorFactory.HUE_RED;
            case 1:
                return BitmapDescriptorFactory.HUE_RED + 10;
            case 2:
                return BitmapDescriptorFactory.HUE_RED + 20;
            case 3:
                return BitmapDescriptorFactory.HUE_YELLOW;
            case 4:
                return BitmapDescriptorFactory.HUE_YELLOW + 10;
            case 5:
                return BitmapDescriptorFactory.HUE_YELLOW + 20;
            case 6:
                return BitmapDescriptorFactory.HUE_GREEN;
            case 7:
                return BitmapDescriptorFactory.HUE_GREEN + 15;
        }
        return BitmapDescriptorFactory.HUE_GREEN + 15;
    }


}
