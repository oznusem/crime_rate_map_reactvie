package com.oznusem.close5exercise.UI;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.oznusem.close5exercise.R;

public class CL5MapsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        CL5MapsFragment mapFragment = (CL5MapsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(mapFragment);
    }
}
