package com.oznusem.close5exercise.Misc;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Oz Nusem on 1/10/16.
 */
public class CL5TimeUtils {

    private static final java.lang.String ISO8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";

    public static String getCurrentTimeISO8601() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(ISO8601_FORMAT, Locale.US);
        return dateFormat.format(cal.getTime());
    }

    public static String getMonthsAgoTimeISO01(int monthsAgo) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(ISO8601_FORMAT,Locale.US);
        cal.add(Calendar.MONTH, -monthsAgo);
        return dateFormat.format(cal.getTime());
    }

}
