package com.oznusem.close5exercise.Models;

/**
 * Created by Oz Nusem on 1/10/16.
 */
public class District {

    private String name;
    private Location location;
    private int numberOfCrimes = 0;
    private int order;

    public District(String name, Location location) {
        this.name = name;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public int getNumberOfCrimes() {
        return numberOfCrimes;
    }

    public void increaseCrimeCount() {
        numberOfCrimes++;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getOrder() {
        return order;
    }
}
