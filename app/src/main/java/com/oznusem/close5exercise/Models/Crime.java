package com.oznusem.close5exercise.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Oz Nusem on 1/9/16.
 */
public class Crime {

    private String time;
    private String category;
    private String pddistrict;
    private String address;
    private String descript;
    private String dayofweek;
    private String resolution;
    private String date;
    private double y;
    private double x;
    private long incidntnum;


    @SerializedName("location")
    private Location location;


    public String getTime() {
        return time;
    }

    public String getCategory() {
        return category;
    }

    public String getPddistrict() {
        return pddistrict;
    }

    public String getAddress() {
        return address;
    }

    public String getDescript() {
        return descript;
    }

    public String getDayofweek() {
        return dayofweek;
    }

    public String getResolution() {
        return resolution;
    }

    public String getDate() {
        return date;
    }

    public double getY() {
        return y;
    }

    public double getX() {
        return x;
    }

    public long getIncidntnum() {
        return incidntnum;
    }

    public Location getLocation() {
        return location;
    }
}
